FROM gradle:jdk11 as builder

COPY --chown=gradle:gradle . /home/gradle/src
ENV GRADLE_OPTS -Dorg.gradle.daemon=false
WORKDIR /home/gradle/src
RUN ./gradlew build

FROM openjdk:11-jre-slim
EXPOSE 8080
COPY --from=builder /home/gradle/src/build/libs/timecode-java-jooby-all-1.0.jar /timecode-java-jooby-all.jar
CMD java -jar /timecode-java-jooby-all.jar